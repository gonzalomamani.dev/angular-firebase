// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAEROLaLFjkzAgmHIbdRq6f9GlDJyyC-tc',
    authDomain: 'usuario-54d06.firebaseapp.com',
    databaseURL: 'https://usuario-54d06.firebaseio.com',
    projectId: 'usuario-54d06',
    storageBucket: 'usuario-54d06.appspot.com',
    messagingSenderId: '557355769040',
    appId: '1:557355769040:web:4cd7984cd85153427c12d5',
    measurementId: 'G-Z1G3XQP1CQ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
