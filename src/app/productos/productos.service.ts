import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Producto} from './producto.model';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ProductosService {
  private itemsCollection: AngularFirestoreCollection<Producto>;
  private itemDoc: AngularFirestoreDocument<Producto>;

  constructor(private afs: AngularFirestore) {
    this.itemsCollection = this.afs.collection<Producto>('productos');
  }
  findAll(): Observable<Producto[]> {
    return this.itemsCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Producto;
        const id = a.payload.doc.id;
        const p: Producto = new Producto();
        p.setAll(Object.assign({ ...data }));
        p.id = id;
        return p;
      }))
    );
/*    return this.itemsCollection.valueChanges().pipe(
      map(response => response as Producto[])
    );*/
  }
  create(producto: Producto) {
    this.itemsCollection.add(JSON.parse(JSON.stringify (producto)));
  }
  update(producto: Producto) {
    this.itemDoc = this.afs.doc<Producto>(`productos/${producto.id}`);
    this.itemDoc.update(JSON.parse(JSON.stringify (producto)));
  }
  delete(producto: Producto) {
    this.itemDoc = this.afs.doc<Producto>(`productos/${producto.id}`);
    this.itemDoc.delete();
  }
}
