import {Component, OnInit, TemplateRef} from '@angular/core';
import {ProductosService} from './productos.service';
import {Producto} from './producto.model';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {
  modalRef: BsModalRef;
  productos: Producto[];
  producto: Producto = new Producto();
  formGroup: FormGroup;
  constructor(private productosService: ProductosService,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.findAll();
    this.buildForm();
  }
  buildForm() {
    this.formGroup = new FormGroup({
      nombre: new FormControl(null, [Validators.required]),
      precio: new FormControl(null, [Validators.required]),
      oferta: new FormControl(null, [Validators.required]),
      fecha: new FormControl(null, [Validators.required]),
    });
  }
  findAll() {
    this.productosService.findAll().subscribe(
      (response) => { this.productos = response; console.log(this.productos); },
      (err) => console.log('Error!!'),
      () => console.log('Complete...')
    );
  }
  submit() {
    console.log(this.formGroup.value);
    if (this.formGroup.valid) {
      if (this.producto.id === null) {
        this.create();
      } else {
        this.update();
      }
    } else {
      alert('Ingrese valores válidos');
    }
  }
  create() {
    this.producto.setAll(this.formGroup.value);
    this.productosService.create(this.producto);
    this.modalRef.hide();
  }
  update() {
    this.producto.setAll(this.formGroup.value);
    this.productosService.update(this.producto);
    this.modalRef.hide();
  }
  delete(producto: Producto) {
    this.productosService.delete(producto);
  }
  openModal(template: TemplateRef<any>, producto: Producto = null) {
    if (producto == null) {
      this.producto = new Producto();
    } else {
      this.producto = producto;
    }
    console.log(producto);
    this.formGroup.patchValue(this.producto);
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-md' }));
  }
}
