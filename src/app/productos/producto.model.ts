export class Producto {
  id: any;
  nombre: string;
  precio: number;
  oferta: boolean;
  fecha: string;
  constructor() {
    this.id = null;
    this.nombre = null;
    this.precio = null;
    this.oferta = null;
    this.fecha = null;
  }
  setAll(values: Producto) {
    this.nombre = values.nombre;
    this.precio = values.precio;
    this.oferta = values.oferta;
    this.fecha = values.fecha;
  }
}
