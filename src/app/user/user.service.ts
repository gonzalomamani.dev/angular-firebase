import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {User} from './user.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private itemsCollection: AngularFirestoreCollection<User>;
  constructor(private afs: AngularFirestore) {}
  serchUsers(): Observable<User[]> {
    this.itemsCollection = this.afs.collection<User>('items');
    return this.itemsCollection.valueChanges().pipe(
      map(response => response as User[])
    );
  }
}
